import time
from random import choice, randint

from POM import SearchPage_POM as search

timeStampFormat = "%m/%d/%Y, %H:%M:%S"


class AndyBot():
    def __init__(self, driver):
        self.driver = driver

    def testFunc(self):
        # START AT: https://www.landlordregistrationscotland.gov.uk/search/start

        searchPage = search.SearchPage(self.driver)

        searchPage.vSwipe()  # 'we will get to the bottom of this!'
        searchPage.vSwipe()
        searchPage.vSwipe()

        searchPage.clickPostcodeRadioButton()
        searchPage.typePostcodeIntoField('g1 1pa')  # do not use caps

        resultsPage = searchPage.clickFindButton()

        resultsPage.clickDropdown()
