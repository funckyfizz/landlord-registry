import random
from functools import wraps
from time import sleep

from POM import Locators as loc
from appium.webdriver.common.touch_action import TouchAction

myDict = {
    '_': 69,  # same as '-' minus symbol but with shift metastate
    'KEYCODE_HOME': 3,
    # Back 'KEY. */,
    'KEYCODE_BACK': 4,
    # '0' 'KEY. */,
    '0': 7,
    # '1' 'KEY. */,
    '1': 8,
    # '2' 'KEY. */,
    '2': 9,
    # '3' 'KEY. */,
    '3': 10,
    # '4' 'KEY. */,
    '4': 11,
    # '5' 'KEY. */,
    '5': 12,
    # '6' 'KEY. */,
    '6': 13,
    # '7' 'KEY. */,
    '7': 14,
    # '8' 'KEY. */,
    '8': 15,
    # '9' 'KEY. */,
    '9': 16,
    # '*' 'KEY. */,
    '*': 17,
    # '#' 'KEY. */,
    '#': 18,
    # Clear 'KEY. */,
    'KEYCODE_CLEAR': 28,
    # 'A' 'KEY. */,
    'a': 29,
    # 'B' 'KEY. */,
    'b': 30,
    # 'C' 'KEY. */,
    'c': 31,
    # 'D' 'KEY. */,
    'd': 32,
    # 'E' 'KEY. */,
    'e': 33,
    # 'F' 'KEY. */,
    'f': 34,
    # 'G' 'KEY. */,
    'g': 35,
    # 'H' 'KEY. */,
    'h': 36,
    # 'I' 'KEY. */,
    'i': 37,
    # 'J' 'KEY. */,
    'j': 38,
    # 'K' 'KEY. */,
    'k': 39,
    # 'L' 'KEY. */,
    'l': 40,
    # 'M' 'KEY. */,
    'm': 41,
    # 'N' 'KEY. */,
    'n': 42,
    # 'O' 'KEY. */,
    'o': 43,
    # 'P' 'KEY. */,
    'p': 44,
    # 'Q' 'KEY. */,
    'q': 45,
    # 'R' 'KEY. */,
    'r': 46,
    # 'S' 'KEY. */,
    's': 47,
    # 'T' 'KEY. */,
    't': 48,
    # 'U' 'KEY. */,
    'u': 49,
    # 'V' 'KEY. */,
    'v': 50,
    # 'W' 'KEY. */,
    'w': 51,
    # 'X' 'KEY. */,
    'x': 52,
    # 'Y' 'KEY. */,
    'y': 53,
    # 'Z' 'KEY. */,
    'z': 54,
    # ',' 'KEY. */,
    ',': 55,
    # '.' 'KEY. */,
    '.': 56,
    # Left Shift modifier 'KEY. */,
    'KEYCODE_SHIFT_LEFT': 59,
    # Right Shift modifier 'KEY. */,
    'KEYCODE_SHIFT_RIGHT': 60,
    # Space 'KEY. */,
    ' ': 62,
    # Enter 'KEY. */,
    'KEYCODE_ENTER': 66,
    # Backspace 'KEY.,
    # *Deletes characters before the insertion point, unlike { @ link  # 'KEYCODE_FORWARD_DEL}. */,
    'KEYCODE_DEL': 67,
    # '`' (backtick) 'KEY. */,
    '`': 68,
    # '-'. */,
    '-': 69,
    # ':' 'KEY. */,
    '=': 70,
    # '[' 'KEY. */,
    '[': 71,
    # ']' 'KEY. */,
    ']': 72,
    # '\' 'KEY. */,
    "\\": 73,
    # '' 'KEY. */,
    ';': 74,
    # ''' (apostrophe) 'KEY. */,
    '\'': 75,
    # '/' 'KEY. */,
    '/': 76,
    # '@' 'KEY. */,
    '@': 77,
    # '+' 'KEY. */,
    '+': 81,
}


def reactionTime(length=1):
    return random.uniform(length * 2, length * 4)


def find_exception_handler(func):
    @wraps(func)
    def inner_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            print(f"{func.__name__} failed")
            print(*args, **kwargs)
            print(f"The cause is: {e}")
            return None

    return inner_function


class Screen():
    def __init__(self, driver):
        self.driver = driver
        self.loc = loc

    @find_exception_handler
    def findElementBy_ID(self, ID):
        return self.driver.find_element_by_id(ID)

    @find_exception_handler
    def findElementsBy_ID(self, ID):
        return self.driver.find_elements_by_id(ID)

    @find_exception_handler
    def findElementBy_XPATH(self, XPATH):
        return self.driver.find_element_by_xpath(XPATH)

    @find_exception_handler
    def findElementsBy_XPATH(self, XPATH):
        return self.driver.find_elements_by_xpath()

    def fastType(self, passage, field):
        field.click()
        for ch in passage:
            field.send_keys(ch)

            sleep(random.uniform(0.080, 0.190))

    def slowType(self, passage, field):
        field.click()

        for ch in passage:
            keyCode = self.getKeycode(ch)
            if keyCode:
                if '_' in ch:
                    self.driver.press_keycode(keyCode, metastate=193)  # shift + '-'
                    continue

                self.driver.press_keycode(keyCode)
            else:
                field.send_keys(ch)

    def getKeycode(self, key):

        keycode = myDict.get(key, None)

        return keycode

    # TODO: clear up your swiping methods
    def getScrollLengthCoordinates(self, length='medium'):
        startX, endX, startY, endY, hold = 0, 0, 0, 0, 0

        if 'med' in length:
            startX = random.randint(750, 850)
            endX = random.randint(750, 850)
            startY = random.randint(1700, 1800)
            endY = random.randint(1000, 1100)
            hold = random.randint(100, 400)

        if 'small' in length:
            startX = random.randint(750, 850)
            endX = random.randint(750, 850)
            startY = random.randint(1500, 1600)
            endY = random.randint(1000, 1250)
            hold = random.randint(300, 600)

        return startX, endX, startY, endY, hold

    def vSwipe(self, length='medium'):
        # Coordinates randomised at start
        startX, endX, startY, endY, hold = self.getScrollLengthCoordinates(length)

        self.driver.swipe(startX, startY, endX, endY, hold)
        self.reactionWait(0.75)

    def reactionWait(self, length=1, verbose=False):
        tminsecs = reactionTime(length)
        if verbose:
            print(f'sleep for {tminsecs}')
        sleep(tminsecs)

    def doubleClick(self, element):
        time_between_clicks = random.uniform(0.050, 0.140)
        element.click()
        sleep(time_between_clicks)
        element.click()
