### searchStart
searchStart_XPath = {
    'rentalPostcode_radioButton': "//android.view.View[1]/android.widget.RadioButton",
    'postCodeSearchField': "//android.view.View[3]/android.view.View/android.view.View/android.widget.EditText",
    'postCodeSearchButton': "//android.view.View[1]/android.view.View[3]/android.view.View/android.widget.Button",
}

### results

results_XPath = {
    'dropdown': '//android.view.View[3]/android.view.View[1]/android.widget.Spinner',
    'Continue_Button': '//android.view.View[5]/android.view.View[3]/android.widget.Button',
    'Back_Button': '//android.view.View[@content-desc="Back"]/android.view.View',
}

results_ID = {
    'dropdown_results': 'org.mozilla.firefox:id/labelView',
}
