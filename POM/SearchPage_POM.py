from POM import Locators as loc
from POM import Screen_POM as screen
from POM import Resutls_POM as results


class SearchPage(screen.Screen):
    # Corresponds to: https://www.landlordregistrationscotland.gov.uk/search/start
    def clickPostcodeRadioButton(self):
        button = self.findElementBy_XPATH(loc.searchStart_XPath['rentalPostcode_radioButton'])
        if button:
            button.click()

    def typePostcodeIntoField(self, query):
        field = self.findElementBy_XPATH(loc.searchStart_XPath['postCodeSearchField'])
        if field:
            self.slowType(query, field)

    def clickFindButton(self):
        button = self.findElementBy_XPATH(loc.searchStart_XPath['postCodeSearchButton'])
        if button:
            button.click()
            return results.ResultsPage(self.driver)
